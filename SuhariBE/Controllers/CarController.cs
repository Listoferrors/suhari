using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuhariBE.Interfaces;
using Microsoft.AspNetCore.Authorization;
using SuhariBE.Models;



namespace SuhariBE.Controllers
{
    
    [Produces("application/json")]
    [Route("cars")]
    public class CarController : Controller
    {
        private readonly ICarRepository _carRepository;

        public CarController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        
        [Route("car")]
        [HttpPost]
        public async Task<IActionResult> PostCar([FromBody]TempCar tempCar)
        {
            await _carRepository.AddCar(tempCar);

            
            return Ok();
        }
        [Route("postjob")]
        [HttpPost]
        public async Task<IActionResult> PostJob([FromBody] Job job)
        {
            if (job != null)
            {
                await _carRepository.AddJob(job);
                return Ok();
            }
            return StatusCode(StatusCodes.Status204NoContent);
        }
        [Route("car")]
        [HttpPut]
        public async Task<IActionResult> UpdateCar([FromBody] TempCar tempCar)
        {
            var job = await CheckForJob(tempCar.id, tempCar);
            if(job == true)
            {
                return Ok();
                                
            }
            else
            {
                return StatusCode(StatusCodes.Status204NoContent);
            }
            
        }
        private async Task<Boolean> CheckForJob(string carId, TempCar tempCar)
        {
            
            
            var taskCheckJobStatus = _carRepository.CheckForJob(carId);
            var jobStatus = await taskCheckJobStatus;
            if(jobStatus)
            {
                await _carRepository.RemoveCar(carId);
                return jobStatus;
            }
            else
            {
                var taskUpdate = _carRepository.UpdateCar(tempCar);
                var derp = await taskUpdate;
                return false;
            }
          
        }
        [Route("job/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetJob(string id)
        {
            TempJob job =  await _carRepository.GetJob(id);

            return Json(job);
            
        }
        [Route("car/{carId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteCar(string carId)
        {
            var result = await _carRepository.RemoveCar(carId);
            if(result.IsAcknowledged)
            {
                return Ok();
            }
            else
            {
                return StatusCode(StatusCodes.Status409Conflict);
            }
        }
        
        [Route("job/{carId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteJob(string carId)
        {
            var result = await _carRepository.RemoveJob(carId);
            if(result.IsAcknowledged)
            {
                return Ok();
            }
            else
            {
                return StatusCode(StatusCodes.Status409Conflict);
            }
        }
        [Route("jobs/update")]
        [HttpGet]
        public async Task<IActionResult> Update()
        {
            await _carRepository.UpdateJobs();
            return Ok();
        }
        
    }
}