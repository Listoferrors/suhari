﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;
//using MongoDB.Bson.Serialization.Attributes;

namespace SuhariBE.Models
{
    public class Job
    {
        [BsonId]
        public Guid Id { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.Now;
        
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        [Required]
        public string Address { get; set; }
        [Required]
        public string Customer { get; set; }
        [Required]
        public string Info { get; set; }
        [Required]
        public double lng { get; set; }
        [Required]
        public double lat { get; set; }
        
        public string CarId { get; set; }


        
    }
}
