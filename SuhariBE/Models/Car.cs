﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
namespace SuhariBE.Models
{
    public class Car
    {
        [Required]
        public string Id { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        //[Required]
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }
      
    }
}
