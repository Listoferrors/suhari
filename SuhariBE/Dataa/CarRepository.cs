﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using SuhariBE.Interfaces;
using SuhariBE.Models;
using MongoDB.Driver.GeoJsonObjectModel;




namespace SuhariBE.Dataa
{
    public class CarRepository : ICarRepository
    {
        private readonly MongoDBContext _context = null;

        public CarRepository(IOptions<Settings> settings)
        {
            _context = new MongoDBContext(settings);
        }
        public async Task<IEnumerable<Car>> GetAllCars()
        {
            try
            {
                return await _context.Cars.Find(_ => true).ToListAsync();
            }
            catch(Exception e)
            {
                //log or manage
                throw e;
            }
        }
        public async Task<Car> GetCar(string id)
        {
            var filter = Builders<Car>.Filter.Eq("Id", id);

            try
            {
                return await _context.Cars
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task AddCar(TempCar tempCar)
        {
            try
            {
                Car car = new Car();
                car.Id = tempCar.id;

                car.Location = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
                new GeoJson2DGeographicCoordinates(tempCar.lon, tempCar.lat));
                await _context.Cars.InsertOneAsync(car);
                
                
            }
            catch(MongoWriteException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
                
            }
        }
        public async Task<UpdateResult> UpdateCar([FromBody] TempCar tempCar)
        {
            Car car = new Car();
            car.Id = tempCar.id;
            car.Location = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
                new GeoJson2DGeographicCoordinates(tempCar.lon, tempCar.lat));
            var filter = Builders<Car>.Filter.Eq(s => s.Id, car.Id);
            var update = Builders<Car>.Update
                            .Set(s => s.Location, car.Location)
                            .Set(s => s.UpdatedOn, DateTime.Now); ;
            try
            {
                return await _context.Cars.UpdateOneAsync(filter, update);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task<Boolean> CheckForJob(string carId)
        {
            var filter = Builders<Job>.Filter.Eq("CarId", carId);
            
            try
            {
                Job job = await _context.Jobs
                                 .Find(filter)
                                 .FirstOrDefaultAsync();
                if(job == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

                //return true;
            }
            catch (Exception ex)
            {
                throw ex;
                // log or manage the exception
                
            }
        }
        public async Task<DeleteResult> RemoveCar(string id)
        {
            try
            {
                return await _context.Cars.DeleteOneAsync(
                     Builders<Car>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        
        public async Task<TempJob> GetJob(string id)
        {
            var filter = Builders<Job>.Filter.Eq("CarId", id);
            
            try
            {
                Job job =  await _context.Jobs
                                  .Find(filter)
                                  .FirstOrDefaultAsync();
                TempJob job2 = new TempJob();
                //TODO edit datetime into proper form
                job2.CreatedOn = job.CreatedOn;
                job2.Address = job.Address;
                job2.Customer = job.Customer;
                job2.Info = job.Info;
                job2.lat = job.lat;
                job2.lng = job.lng;
                return job2;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DeleteResult> RemoveJob(string carId)
        {
            try
            {
                return await _context.Jobs.DeleteOneAsync(
                     Builders<Job>.Filter.Eq("CarId", carId));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task AddJob(Job job)
        {
            var car = await FindClosestCar(job);
            
            if(car != null)
            {
                job.CarId = car.Id;
                var delCar = await RemoveCar(car.Id);

            }
            else
            {
                job.CarId = null;
            }

            try
            {
                await _context.Jobs.InsertOneAsync(job);
                
            }
            catch(Exception e)
            {
                throw e;
            }
            
        }
        
        public async Task<Car> FindClosestCar(Job job)
        {
            var point = GeoJson.Point(GeoJson.Geographic(job.lng, job.lat));
            var locationQ = new FilterDefinitionBuilder<Car>().Near(t => t.Location, point, 200000);

            try
            {
                return await _context.Cars.Find(locationQ).FirstAsync();


            }
            catch(Exception e)
            {
                return null;
                throw e;
            }
        }
        

        public async Task<ReplaceOneResult> UpdateCar(string id, Car item)
        {
            try
            {
                return await _context.Cars
                            .ReplaceOneAsync(n => n.Id.Equals(id), 
                                        item, 
                                        new UpdateOptions { IsUpsert = true });
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        //gets called by the backgroundprocess
        public async Task UpdateJobs()
        {
            var filter = Builders<Job>.Filter.Lt(p => p.UpdatedOn,
                DateTime.UtcNow.AddMinutes(-1).AddSeconds(-15));
            var jobResult = await _context.Jobs.FindAsync(filter);

            if (jobResult != null)
            {
                foreach (var job in jobResult.ToListAsync().Result)
                {

                    try
                    {
                        var point = GeoJson.Point(GeoJson.Geographic(job.lng, job.lat));
                        var locationQ = new FilterDefinitionBuilder<Car>()
                            .Near(t => t.Location, point, 200000);
                        Car car = _context.Cars.Find(locationQ).FirstAsync().Result;

                        if (car != null)
                        {
                            var nextCarId = car.Id;
                            //if the closest car is the same one
                            await _context.Cars.DeleteOneAsync(Builders<Car>.Filter.Eq("Id", car.Id));
                            job.UpdatedOn = DateTime.Now;
                            var jobFilter = Builders<Job>.Filter.Eq(s => s.Id, job.Id);
                            var jobUpdate = Builders<Job>.Update
                                .Set(s => s.CarId, nextCarId)
                                .Set(x => x.UpdatedOn, DateTime.Now);
                            await _context.Jobs.UpdateOneAsync(jobFilter, jobUpdate);
                            

                        }
                    }
                    catch (InvalidOperationException e)
                    {
                        var jobFilter = Builders<Job>.Filter.Eq(s => s.Id, job.Id);
                        var jobUpdate = Builders<Job>.Update
                            .Set(s => s.CarId, null)
                            .Set(x => x.UpdatedOn, DateTime.Now);
                        await _context.Jobs.UpdateOneAsync(jobFilter, jobUpdate);   

                    }
                    catch (Exception e)
                    {
                        //log it
                    }

                }
            }
           
        }
       
    }
}
