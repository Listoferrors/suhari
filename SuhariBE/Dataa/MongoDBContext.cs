﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SuhariBE.Models;



namespace SuhariBE
{
    public class MongoDBContext
    {
        private IMongoDatabase _database = null;

        public MongoDBContext(IOptions<Settings> settings)
        {
            
            var client = new MongoClient(settings.Value.ConnectionString);
           
            if(client != null)
            {
                _database = client.GetDatabase(settings.Value.DatabaseName);
            }
                
            var collection = _database.GetCollection<Car>("Cars");
            //create index for geoqueries
            collection.Indexes.CreateOneAsync(Builders<Car>.IndexKeys
                .Geo2DSphere(i => i.Location));
            
        }
        public IMongoCollection<Job> Jobs
        {
            get
            {
                return _database.GetCollection<Job>("Jobs");
            }
        }
        public IMongoCollection<Car> Cars
        {
            get
            {
                return _database.GetCollection<Car>("Cars");
                
            }
        }
        
    }
    
}
