﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SuhariBE.Models;
using MongoDB.Driver;
namespace SuhariBE.Interfaces
{
    
    public interface ICarRepository
    {
        Task<IEnumerable<Car>> GetAllCars();
        Task<Car> GetCar(string id);
        Task AddCar(TempCar tempCar);
        Task<DeleteResult> RemoveCar(string id);
        Task AddJob(Job job);
        Task<Car> FindClosestCar(Job job);
        Task<Boolean> CheckForJob(string carId);
        Task<UpdateResult> UpdateCar(TempCar tempCar);
        Task<TempJob> GetJob(string id);
        Task<DeleteResult> RemoveJob(string carId);
        Task UpdateJobs();
       
    }
}
